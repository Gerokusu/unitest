using System;
using UnityEditor;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerToolbarItem
	{
		public string Name { get; set; }
		public bool Enabled { get; set; }
		public event Action<TestRunnerToolbarItem, EventArgs> OnClick;

		public TestRunnerToolbarItem(string name, bool enabled = true)
		{
			Name = name;
			Enabled = enabled;
		}

		public float GetWidth()
		{
			return new GUIStyle(EditorStyles.toolbarButton).CalcSize(new GUIContent(Name)).x;
		}

		public void OnGUI(Rect rect)
		{
			GUILayout.BeginArea(rect);
			if (Enabled && GUILayout.Button(new GUIContent(Name), EditorStyles.toolbarButton))
			{
				OnClick?.Invoke(this, new EventArgs());
			}
			else
			{
				GUIStyle style = new GUIStyle(EditorStyles.toolbarButton);
				style.normal.textColor = Color.grey;

				GUILayout.Label(new GUIContent(Name), style);
			}
			GUILayout.EndArea();
		}
	}
}