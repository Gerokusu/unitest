using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerTreeViewItem : TreeViewItem
	{
		public static int UID { get; protected set; }
		protected static readonly Dictionary<TestStageStatus, Texture2D> IconTextures;

		public TestStage Stage { get; protected set; }
		public string Name { get; protected set; }
		public TestStageStatus Status { get; protected set; }
		public string Message { get; protected set; }
		public float Duration { get; protected set; }

		public TestRunnerTreeViewItem(TestStage stage) : base()
		{
			Stage = stage;
			Stage.OnStatusUpdate += (sender, args) =>
			{
				SetData(sender.Name, args.Status, args.Message, args.Duration, args.IsError);
			};

			SetData(Stage.Name, TestStageStatus.Normal, "", 0, false);
		}

		protected void SetData(string name, TestStageStatus status, string message, float duration, bool isError)
		{
			Name = name.Trim();
			icon = IconTextures[status];
			Status = status;
			Message = message.Trim();
			Duration = duration;
			if (isError)
			{
				Debug.LogError(Message);
			}
		}

		static TestRunnerTreeViewItem()
		{
			UID = 0;
			IconTextures = Enum.GetValues(typeof(TestStageStatus))
			.OfType<TestStageStatus>()
			.ToDictionary
			(
				status => status,
				status => (Texture2D)EditorGUIUtility.IconContent("Test" + status.ToString()).image
			);
		}

		public static TestRunnerTreeViewItem BuildFromTestStage(TestStage stage, int depth, TestRunnerTreeViewItem parent = null)
		{
			TestRunnerTreeViewItem item = new TestRunnerTreeViewItem(stage);
			item.id = ++UID;
			item.depth = depth;
			item.parent = parent;
			item.children = stage.Children.Select(child => (TreeViewItem)BuildFromTestStage(child, depth + 1, item)).ToList();

			return item;
		}
	}
}