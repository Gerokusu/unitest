using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerWindow : EditorWindow
	{
		public static readonly Color COLOR_LABEL_NORMAL = new Color32(196, 196, 196, 255);
		public static readonly Color COLOR_LABEL_DESCRIPTION = new Color32(118, 118, 118, 255);
		public static readonly Color COLOR_LABEL_ERROR = new Color32(211, 34, 34, 255);
		public static readonly Color COLOR_LABEL_ERROR_BACKGROUND = new Color32(25, 25, 25, 255);

		protected TestRunnerToolbar toolbar;
		protected TestRunnerToolbarItem toolbarItemClear;
		protected TestRunnerToolbarItem toolbarItemDiscover;
		protected TestRunnerToolbarItem toolbarItemRun;
		protected TestRunnerToolbarItem toolbarItemRunSelected;
		protected TestRunnerTreeView treeView;
		protected TestRunnerDescription description;

		public void OnEnable()
		{
			titleContent = new GUIContent("Unitest Runner") { image = (Texture2D)EditorGUIUtility.Load("Assets/GS/Unitest.Editor/Icons/Flask.small.png") };

			toolbar = new TestRunnerToolbar();
			toolbar.OnSearch += (sender, args) =>
			{
				treeView.searchString = args.Text;
			};

			toolbarItemClear = toolbar.AddItem("Clear", (sender, args) => Clear());
			toolbarItemDiscover = toolbar.AddItem("Discover", (sender, args) => Discover());
			toolbarItemRun = toolbar.AddItem("Run", (sender, args) => Run());
			toolbarItemRun.Enabled = false;
			toolbarItemRunSelected = toolbar.AddItem("Run selected", (sender, args) => RunSelected());
			toolbarItemRunSelected.Enabled = false;

			treeView = new TestRunnerTreeView(new TreeViewState());
			treeView.OnSelectionChanged += (sender, args) =>
			{
				description.title = args.Item != null ? args.Item.Name : "";
				description.content = args.Item != null ? args.Item.Message : "";
				toolbarItemRunSelected.Enabled = treeView.SelectedItem != null;
			};

			description = new TestRunnerDescription();

			Discover();
		}

		public void Clear()
		{
			TestEditor.Clear();
			toolbarItemRun.Enabled = false;
			treeView.ClearSelection();
			treeView.Reload();
		}

		public void Discover()
		{
			TestEditor.Discover();
			toolbarItemRun.Enabled = TestEditor.Root != null && TestEditor.Root.Children.Count > 0;
			treeView.ClearSelection();
			treeView.Reload();
			treeView.ExpandAll();
		}

		public void Run()
		{
			TestEditor.Run(TestEditor.Root);
			treeView.Repaint();
		}

		public void RunSelected()
		{
			TestEditor.Run(treeView.SelectedItem?.Stage);
			treeView.Repaint();
		}

		public void OnGUI()
		{
			float x = 0;
			float y = 0;
			float width = position.width;
			Rect toolbarRect = new Rect(x, y, width, 20);
			Rect testRunnerViewRect = new Rect(x, y += toolbarRect.height, width, position.height - y - 70);
			Rect testRunnerDescriptionRect = new Rect(x, y += testRunnerViewRect.height, width, 70);

			OnGUIToolbar(toolbarRect);
			OnGUITreeView(testRunnerViewRect, 0, new Color(0.16F, 0.16F, 0.16F, 1));
			OnGUIDescription(testRunnerDescriptionRect);
		}

		protected void OnGUIToolbar(Rect rect)
		{
			toolbar.OnGUI(rect);
		}

		protected void OnGUITreeView(Rect rect, uint borderWidth, Color borderColor)
		{
			treeView.OnGUI(rect);
		}

		protected void OnGUIDescription(Rect rect)
		{
			description.OnGUI(rect);
		}
	}
}