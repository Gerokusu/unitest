using System;
using System.Collections.Generic;
using GS.UI.Editor;
using UnityEditor;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerToolbar
	{
		protected TextFieldSearch textFieldSearch;
		protected List<TestRunnerToolbarItem> Items { get; set; }
		public event Action<TestRunnerToolbar, TextFieldEventArgs> OnSearch;

		public TestRunnerToolbar(params (string, bool, Action<object, EventArgs>)[] items)
		{
			Items = new List<TestRunnerToolbarItem>();
			textFieldSearch = new TextFieldSearch();
			textFieldSearch.OnTextChange += (sender, args) =>
			{
				OnSearch?.Invoke(this, args);
			};
		}

		public TestRunnerToolbarItem AddItem(string name, Action<object, EventArgs> callback)
		{
			TestRunnerToolbarItem item = new TestRunnerToolbarItem(name);
			item.OnClick += callback;

			Items.Add(item);

			return item;
		}

		public void OnGUI(Rect rect)
		{
			GUILayout.BeginArea(rect, EditorStyles.toolbar);

			float x = rect.x;
			foreach (TestRunnerToolbarItem item in Items)
			{
				Rect rectItem = new Rect(x, rect.y, item.GetWidth(), rect.height);
				x += rectItem.width;

				item.OnGUI(rectItem);
			}

			textFieldSearch.Render(new Rect(rect.width - 200, rect.y, 200, rect.height));

			GUILayout.EndArea();
		}
	}
}