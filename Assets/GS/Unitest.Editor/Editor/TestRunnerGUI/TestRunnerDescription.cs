using UnityEditor;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerDescription
	{
		public string title = "";
		public string content = "";
		protected Vector2 scrollPosition = Vector2.zero;

		public void OnGUI(Rect rect)
		{
			GUILayout.BeginArea(rect);

			scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false);

			GUILayout.Label(new GUIContent(title), new GUIStyle(EditorStyles.label) { fontStyle = FontStyle.Bold, });
			GUILayout.Label(new GUIContent(content), new GUIStyle(EditorStyles.label) { richText = true });

			GUILayout.EndScrollView();

			GUILayout.EndArea();
		}
	}
}