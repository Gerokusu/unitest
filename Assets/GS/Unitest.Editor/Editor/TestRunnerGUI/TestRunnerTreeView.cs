using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerTreeView : TreeView
	{
		protected int SelectedId { get; set; }
		public TestRunnerTreeViewItem SelectedItem => (TestRunnerTreeViewItem)FindItem(SelectedId, rootItem);
		public event Action<TestRunnerTreeView, TestRunnerTreeViewEventArgs> OnSelectionChanged;

		public TestRunnerTreeView(TreeViewState treeViewState) : base(treeViewState)
		{
			showAlternatingRowBackgrounds = true;
			showBorder = true;
			SelectedId = -1;
			Reload();
		}

		public void ClearSelection()
		{
			SingleClickedItem(-1);
		}

		protected override TreeViewItem BuildRoot()
		{
			if (TestEditor.Root != null)
			{
				return TestRunnerTreeViewItem.BuildFromTestStage(TestEditor.Root, -1);
			}
			else
			{
				return new TreeViewItem(0, -1) { children = new List<TreeViewItem>() };
			}
		}

		protected override bool DoesItemMatchSearch(TreeViewItem item, string search)
		{
			search = search.ToLower().Trim();
			return DoesItemMatchSearchRecursive(item, search) && item.children.Count == 0;
		}

		protected bool DoesItemMatchSearchRecursive(TreeViewItem item, string search)
		{
			bool doesItemMatch = ((TestRunnerTreeViewItem)item).Name.ToLower().Contains(search);
			bool hasParentMatching = item.parent != null && DoesItemMatchSearchRecursive(item.parent, search);
			return doesItemMatch || hasParentMatching;
		}

		protected override bool CanMultiSelect(TreeViewItem item)
		{
			return false;
		}

		protected override void SingleClickedItem(int id)
		{
			if (SelectedId == id)
			{
				SetSelection(new List<int>());
			}

			SelectedId = id;
			OnSelectionChanged?.Invoke(this, new TestRunnerTreeViewEventArgs(SelectedItem));
		}

		protected override void RowGUI(RowGUIArgs args)
		{
			base.RowGUI(args);

			TestRunnerTreeViewItem item = (TestRunnerTreeViewItem)args.item;
			item.displayName = "";

			float cursor = 16 + GetContentIndent(item);

			if (item.Name.Length > 0)
			{
				string prefix = item.children.Count > 0 ? "When" : "It";
				cursor += RowGUISeparator();
				cursor += RowGUILabel(cursor, args.row, string.Format("<b>{0}</b> {1}", prefix, item.Name), TestRunnerWindow.COLOR_LABEL_NORMAL);
			}

			if (item.Duration > 0)
			{
				cursor += RowGUISeparator();
				cursor += RowGUILabel(cursor, args.row, "•", TestRunnerWindow.COLOR_LABEL_DESCRIPTION);
				cursor += RowGUISeparator();
				cursor += RowGUILabel(cursor, args.row, item.Duration.ToString("0.0000") + "ms", TestRunnerWindow.COLOR_LABEL_DESCRIPTION);
			}

			if (item.Message.Length > 0)
			{
				cursor += RowGUISeparator();
				cursor += RowGUILabel(cursor, args.row, "#", TestRunnerWindow.COLOR_LABEL_DESCRIPTION);
				cursor += RowGUISeparator();
				if (item.Status == TestStageStatus.Failed)
				{
					int indexOfBreak = item.Message.IndexOf('\n');
					string message = indexOfBreak > 0 ? item.Message.Substring(0, item.Message.IndexOf('\n')).Trim() : item.Message;
					cursor += RowGUILabel(cursor, args.row, message, TestRunnerWindow.COLOR_LABEL_ERROR, TestRunnerWindow.COLOR_LABEL_ERROR_BACKGROUND);
				}
				else
				{
					cursor += RowGUILabel(cursor, args.row, item.Message, TestRunnerWindow.COLOR_LABEL_DESCRIPTION);
				}
			}
		}

		protected int RowGUILabel(float offset, int rowNumber, string text, Color textColor, Color32? backgroundColor = null)
		{
			GUIContent label = new GUIContent(text);

			GUIStyle style = new GUIStyle(EditorStyles.label);
			style.richText = true;
			style.normal.textColor = textColor;

			Rect rect = new Rect(offset, rowNumber * rowHeight, style.CalcSize(label).x, rowHeight);
			if (backgroundColor != null && backgroundColor.HasValue)
			{
				EditorGUI.DrawRect(rect, backgroundColor.Value);
			}

			EditorGUI.LabelField(rect, label, style);

			return Mathf.CeilToInt(rect.width);
		}

		protected int RowGUISeparator()
		{
			return Mathf.CeilToInt(new GUIStyle(EditorStyles.label).CalcSize(new GUIContent(" ")).x);
		}
	}
}