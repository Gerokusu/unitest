namespace GS.Unitest.Editor.TestRunnerGUI
{
	public class TestRunnerTreeViewEventArgs
	{
		public TestRunnerTreeViewItem Item { get; protected set; }

		public TestRunnerTreeViewEventArgs(TestRunnerTreeViewItem item)
		{
			Item = item;
		}
	}
}