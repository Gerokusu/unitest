using System.Linq;
using System.Reflection;
using GS.Unitest.Editor.TestRunnerGUI;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GS.Unitest.Editor
{
	public static class TestEditor
	{
		public static TestStage Root { get; set; }

		[MenuItem("Window/Unitest/Show Runner", false, 3030)]
		public static void ShowRunner()
		{
			TestRunnerWindow window = EditorWindow.GetWindow<TestRunnerWindow>();
			window.Show();
		}

		public static void Clear()
		{
			Root = null;
		}

		public static void Discover()
		{
			Assembly[] assemblies = AssetDatabase.FindAssets(string.Format("t:AssemblyDefinitionAsset"), new string[] { "Assets" })
			.Select(assetGuid => AssetDatabase.GUIDToAssetPath(assetGuid))
			.Select(assetPath => AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>(assetPath))
			.Select(asset => Assembly.Load(asset.name))
			.ToArray();

			Root = TestStage.BuildFromAssemblies(assemblies);
		}

		public static void Run(TestStage stage)
		{
			Scene scene = EditorSceneManager.GetActiveScene();
			if (scene.path.Trim() != "")
			{
				string path = scene.path;
				EditorSceneManager.SaveScene(scene, path);
				EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

				stage.Run();

				EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
			}
			else
			{
				Debug.LogError("Please save the scene before running any test.");
			}
		}
	}
}