using System;

namespace GS.UI.Editor
{
	public class TextFieldEventArgs : EventArgs
	{
		public string Text { get; protected set; }

		public TextFieldEventArgs(string text)
		{
			Text = text;
		}
	}
}