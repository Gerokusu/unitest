using UnityEditor;
using UnityEngine;

namespace GS.UI.Editor
{
	public class TextFieldSearch : TextField
	{
		public override GUIStyle Style => EditorStyles.toolbarSearchField;
	}
}