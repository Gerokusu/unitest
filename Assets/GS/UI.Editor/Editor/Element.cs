using System;
using UnityEngine;

namespace GS.UI.Editor
{
	public abstract class Element
	{
		public abstract GUIStyle Style { get; }

		public bool Render(Rect rect)
		{
			bool hasRendered = false;
			try
			{
				OnRender(rect);
				hasRendered = true;
			}
			catch (Exception exception)
			{
				Debug.LogError(exception);
			}

			return hasRendered;
		}

		protected abstract void OnRender(Rect rect);
	}
}