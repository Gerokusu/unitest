using System;
using UnityEditor;
using UnityEngine;

namespace GS.UI.Editor
{
	public class TextField : Element
	{
		public override GUIStyle Style => EditorStyles.textField;

		public string Text { get; protected set; }
		public event Action<TextField, TextFieldEventArgs> OnTextChange;

		public TextField(string textDefault = "") : base()
		{
			Text = textDefault;
		}

		protected override void OnRender(Rect rect)
		{
			float height = Style.fixedHeight > 0 ? Style.fixedHeight : rect.height;
			float y = (rect.height - height) / 2;
			GUILayout.BeginArea(rect);
			Text = EditorGUI.TextField(new Rect(1, y, rect.width - 2, height), Text, Style);
			OnTextChange?.Invoke(this, new TextFieldEventArgs(Text));
			GUILayout.EndArea();
		}
	}
}