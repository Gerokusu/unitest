using System;
using GS.Unitest.Assertions;
using GS.Unitest.Examples.Dummies;

namespace GS.Unitest.Examples
{
	public class ExampleTest : TestStage
	{
		protected DateTime ExampleDate { get; set; }
		protected DummyStruct DummyStruct { get; set; }
		protected DummyClass DummyClass { get; set; }

		protected override void Test()
		{
			When("the tested value is a boolean", () =>
			{
				It("asserts Is if actual is equal to expected", () =>
				{
					Assert(true).Is(true);
					Assert(true).Not.Is(false);
				});
			});

			When("the tested value is a number", () =>
			{
				It("asserts Is if actual is equal to expected (byte)", () =>
				{
					Assert((byte)2).Is((byte)2);
					Assert((byte)2).Not.Is((byte)3);
				});

				It("asserts Is if actual is equal to expected (sbyte)", () =>
				{
					Assert((sbyte)2).Is((sbyte)2);
					Assert((sbyte)2).Not.Is((sbyte)3);
				});

				It("asserts Is if actual is equal to expected (short)", () =>
				{
					Assert((short)2).Is((short)2);
					Assert((short)2).Not.Is((short)3);
				});

				It("asserts Is if actual is equal to expected (ushort)", () =>
				{
					Assert((ushort)2).Is((ushort)2);
					Assert((ushort)2).Not.Is((ushort)3);
				});

				It("asserts Is if actual is equal to expected (int)", () =>
				{
					Assert((int)2).Is((int)2);
					Assert((int)2).Not.Is((int)3);
				});

				It("asserts Is if actual is equal to expected (uint)", () =>
				{
					Assert((uint)2).Is((uint)2);
					Assert((uint)2).Not.Is((uint)3);
				});

				It("asserts Is if actual is equal to expected (long)", () =>
				{
					Assert((long)2).Is((long)2);
					Assert((long)2).Not.Is((long)3);
				});

				It("asserts Is if actual is equal to expected (ulong)", () =>
				{
					Assert((ulong)2).Is((ulong)2);
					Assert((ulong)2).Not.Is((ulong)3);
				});

				It("asserts Is if actual is equal to expected (decimal)", () =>
				{
					Assert((decimal)2).Is((decimal)2);
					Assert((decimal)2).Not.Is((decimal)3);
				});

				It("asserts Is if actual is equal to expected (float)", () =>
				{
					Assert((float)2).Is((float)2);
					Assert((float)2).Not.Is((float)3);
				});

				It("asserts Is if actual is equal to expected (double)", () =>
				{
					Assert((double)2).Is((double)2);
					Assert((double)2).Not.Is((double)3);
				});

				It("asserts IsLesserThan if actual is lesser than expected", () =>
				{
					Assert(0).IsLesserThan(4);
					Assert(0).Not.IsLesserThan(0);
					Assert(4).Not.IsLesserThan(0);
				});

				It("asserts IsLesserThanOrEqual if actual is lesser than or equal to expected", () =>
				{
					Assert(0).IsLesserThanOrEqualTo(4);
					Assert(0).IsLesserThanOrEqualTo(0);
					Assert(4).Not.IsLesserThanOrEqualTo(0);
				});

				It("asserts IsGreaterThan if actual is greater than expected", () =>
				{
					Assert(4).IsGreaterThan(0);
					Assert(0).Not.IsGreaterThan(0);
					Assert(0).Not.IsGreaterThan(4);
				});

				It("asserts IsGreaterThanOrEqualTo if actual is greater than or equal to expected", () =>
				{
					Assert(4).IsGreaterThanOrEqualTo(0);
					Assert(0).IsGreaterThanOrEqualTo(0);
					Assert(0).Not.IsGreaterThanOrEqualTo(4);
				});
			});

			When("the tested value is a character", () =>
			{
				It("asserts Is if actual is equal to expected", () =>
				{
					Assert('a').Is('a');
					Assert('a').Not.Is('b');
				});
			});

			When("the tested value is a string", () =>
			{
				It("asserts Is if actual is equal to expected", () =>
				{
					Assert("a").Is("a");
					Assert("a").Not.Is("b");
				});
			});

			When("the tested value is a DateTime", () =>
			{
				BeforeEach(() =>
				{
					ExampleDate = new DateTime(2020, 03, 17, 14, 41, 44, 270);
				});

				It("asserts Is if actual is equal to expected", () =>
				{
					Assert(ExampleDate).Is(new DateTime(2020, 03, 17, 14, 41, 44, 270));
					Assert(ExampleDate).Not.Is(new DateTime(2020, 03, 17, 14, 41, 44, 251));
				});

				It("asserts IsLesserThan if actual is lesser than expected", () =>
				{
					Assert(ExampleDate).IsLesserThan(new DateTime(2020, 03, 21, 8, 16, 20, 800));
					Assert(ExampleDate).Not.IsLesserThan(new DateTime(2020, 03, 17, 14, 41, 44, 270));
					Assert(ExampleDate).Not.IsLesserThan(new DateTime(2018, 12, 3, 19, 51, 34, 561));
				});

				It("asserts IsLesserThanOrEqual if actual is lesser than or equal to expected", () =>
				{
					Assert(ExampleDate).IsLesserThanOrEqualTo(new DateTime(2020, 03, 21, 8, 16, 20, 800));
					Assert(ExampleDate).IsLesserThanOrEqualTo(new DateTime(2020, 03, 17, 14, 41, 44, 270));
					Assert(ExampleDate).Not.IsLesserThanOrEqualTo(new DateTime(2018, 12, 3, 19, 51, 34, 561));
				});

				It("asserts IsGreaterThan if actual is greater than expected", () =>
				{
					Assert(ExampleDate).IsGreaterThan(new DateTime(2018, 12, 3, 19, 51, 34, 561));
					Assert(ExampleDate).Not.IsGreaterThan(new DateTime(2020, 03, 17, 14, 41, 44, 270));
					Assert(ExampleDate).Not.IsGreaterThan(new DateTime(2020, 03, 21, 8, 16, 20, 800));
				});

				It("asserts IsGreaterThanOrEqualTo if actual is greater than or equal to expected", () =>
				{
					Assert(ExampleDate).IsGreaterThanOrEqualTo(new DateTime(2018, 12, 3, 19, 51, 34, 561));
					Assert(ExampleDate).IsGreaterThanOrEqualTo(new DateTime(2020, 03, 17, 14, 41, 44, 270));
					Assert(ExampleDate).Not.IsGreaterThanOrEqualTo(new DateTime(2020, 03, 21, 8, 16, 20, 800));
				});
			});

			When("the tested value is a structure", () =>
			{
				BeforeEach(() =>
				{
					DummyStruct = new DummyStruct(2, 4);
				});

				It("asserts Is if actual is equal to expected", () =>
				{
					Assert(DummyStruct).Is(new DummyStruct(2, 4));
					Assert(DummyStruct).Not.Is(new DummyStruct(2, 5));
				});
			});

			When("the tested value is a class instance", () =>
			{
				BeforeEach(() =>
				{
					DummyClass = new DummyClass(2, 4);
				});

				It("asserts Is if actual is equal to expected", () =>
				{
					Assert(DummyClass).Is(new DummyClass(2, 4));
					Assert(DummyClass).Not.Is(new DummyClass(2, 5));
				});

				It("asserts IsReference if actual has the same reference as expected", () =>
				{
					DummyClass exampleClassOther = DummyClass;
					Assert(DummyClass).IsReference(exampleClassOther);
					Assert(DummyClass).Not.IsReference(new DummyClass(2, 4));
				});

				It("asserts IsNull if actual is null", () =>
				{
					DummyClass exampleClassOther = null;
					Assert(exampleClassOther).IsNull();
					Assert(DummyClass).Not.IsNull();
				});
			});

			When("the tested value is an action", () =>
			{
				It("asserts an exception was raised in the actual action", () =>
				{
					Assert(() => { throw new Exception(); }).Raises<Exception>();
					Assert(() => { }).Not.Raises<Exception>();
				});

				It("asserts an exception was raised in the actual action with the expected message", () =>
				{
					Assert(() => { throw new Exception("This is an exception"); }).Raises<Exception>("This is an exception");
					Assert(() => { throw new Exception("This is another exception"); }).Not.Raises<Exception>("This is an exception");
				});
			});
		}
	}
}