using GS.Unitest.Assertions;
using GS.Unitest.Examples.Dummies;
using GS.Unitest.Mocks;

namespace GS.Unitest.Examples
{
	public class ExampleMock : TestStage
	{
		protected DummyClass DummyClass { get; set; }

		protected override void Test()
		{
			When("the tested value is a mock", () =>
			{
				DummyClassWithDependance instance = null;
				Mock<DummyClass> mockedDummyClass = null;
				Mock<DummyClass> mockedDummyClassNotCallingDependance = null;

				BeforeEach(() =>
				{
					instance = new DummyClassWithDependance();
					mockedDummyClass = new Mock<DummyClass>();
					mockedDummyClassNotCallingDependance = new Mock<DummyClass>();
					mockedDummyClassNotCallingDependance.Spy("ExampleMethod");

					instance.DummyClass = mockedDummyClass.Spy("ExampleMethod2");
					instance.ExampleMethodCallingMockedDependance();
				});

				It("asserts HasBeenCalled if the mocked method has been called", () =>
				{
					Assert(mockedDummyClass).HasBeenCalled();
					Assert(mockedDummyClassNotCallingDependance).Not.HasBeenCalled();
				});

				It("asserts HasBeenCalledTimes if the mocked method has been called a given number of times", () =>
				{
					instance.ExampleMethodCallingMockedDependance();
					instance.ExampleMethodCallingMockedDependance();

					Assert(mockedDummyClass).HasBeenCalledTimes(3);
					Assert(mockedDummyClass).Not.HasBeenCalledTimes(2);
				});

				It("asserts HasBeenCalledWith if the mocked method has been called with the given parameters", () =>
				{
					Assert(mockedDummyClass).HasBeenCalledWith(20, 12, "test");
					Assert(mockedDummyClass).Not.HasBeenCalledWith(0, 1, "other test");
				});
			});
		}
	}
}