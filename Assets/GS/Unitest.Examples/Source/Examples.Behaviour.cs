
using GS.Unitest.Examples.Dummies;
using UnityEngine;

namespace GS.Unitest.Examples
{
	public class ExampleBehaviour : TestStage
	{
		DummyBehaviour behaviour;

		protected override void Test()
		{
			BeforeEach(() =>
			{
				behaviour = new GameObject("toto").AddComponent<DummyBehaviour>();
				behaviour.Start();
			});

			It("calls start", () =>
			{
				Debug.Log("toto");
			});
		}
	}
}