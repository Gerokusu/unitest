namespace GS.Unitest.Examples.Dummies
{
	public class DummyClassWithDependance
	{
		public DummyClass DummyClass { get; set; }

		public DummyClassWithDependance()
		{
			DummyClass = new DummyClass();
		}

		public void ExampleMethodCallingDependance()
		{
			DummyClass.ExampleMethod();
		}

		public void ExampleMethodCallingMockedDependance()
		{
			DummyClass.ExampleMethod2(20, 12, "test");
		}
	}
}