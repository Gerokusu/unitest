using UnityEngine;

namespace GS.Unitest.Examples.Dummies
{
	public class DummyBehaviour : MonoBehaviour
	{
		public void Start()
		{
			Debug.Log("start");
		}

		public void Update()
		{
			Debug.Log("update");
		}
	}
}