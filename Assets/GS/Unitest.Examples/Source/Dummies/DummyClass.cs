namespace GS.Unitest.Examples.Dummies
{
	public class DummyClass
	{
		public int A { get; set; }
		public int B { get; set; }
		protected string C { get; set; }

		public DummyClass()
		{
			A = 0;
			B = 0;
		}

		public DummyClass(int a, int b)
		{
			A = a;
			B = b;
		}

		public virtual void ExampleMethod()
		{
			A++;
			B++;
		}

		public virtual void ExampleMethod2(int a, int b, string c)
		{
			A = a;
			B = b;
			C = c;
		}

		public override bool Equals(object other)
		{
			return other.GetType() == typeof(DummyClass) && ((DummyClass)other).A == A && ((DummyClass)other).B == B;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}