namespace GS.Unitest.Examples.Dummies
{
	public struct DummyStruct
	{
		public int A { get; set; }
		public int B { get; set; }

		public DummyStruct(int a, int b)
		{
			A = a;
			B = b;
		}
	}
}