namespace GS.Unitest.Assertions
{
	public static class AssertionCommon
	{
		public static void Is<T>(this Assertion<T> assertion, T expected)
		{
			Assertion<T>.Verify(assertion, expected, (a, b) => object.Equals(a, b));
		}
	}
}