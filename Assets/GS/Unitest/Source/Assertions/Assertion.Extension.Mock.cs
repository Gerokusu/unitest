using System.Linq;
using GS.Unitest.Mocks;

namespace GS.Unitest.Assertions
{
	public static class AssertionMock
	{
		public static void HasBeenCalled<T>(this Assertion<Mock<T>> assertion) where T : class
		{
			Assertion<Mock<T>>.Verify(assertion, (actual) => actual.GetCalls().Length > 0);
		}

		public static void HasBeenCalledTimes<T>(this Assertion<Mock<T>> assertion, int expected) where T : class
		{
			Assertion<Mock<T>>.Verify(assertion, (actual) => actual.GetCalls().Length == expected);
		}

		public static void HasBeenCalledWith<T>(this Assertion<Mock<T>> assertion, params object[] expected) where T : class
		{
			Assertion<Mock<T>>.Verify(assertion, expected, (a, b) => a.GetCalls().Any(arguments => arguments.SequenceEqual(b)));
		}
	}
}