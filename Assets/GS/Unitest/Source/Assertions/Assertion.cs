using System;
using System.Linq;

namespace GS.Unitest.Assertions
{
	public abstract class Assertion
	{
		public static string GetFormattedVariable(object variable)
		{
			string result = "";
			if (variable != null)
			{
				if (variable.GetType().IsArray)
				{
					string[] items = ((object[])variable).Select(item => GetFormattedVariable(item)).ToArray();
					result = string.Format("[{0}]", string.Join(", ", items));
				}
				else
				{
					result = variable.GetType() == typeof(string) ? string.Format("\"{0}\"", variable) : variable.ToString();
				}
			}
			else
			{
				result = "null";
			}

			return result;
		}
	}

	public class Assertion<T> : Assertion
	{
		protected bool shouldAssertTrue = true;

		public T Actual { get; protected set; }
		public Assertion<T> Not { get { shouldAssertTrue = !shouldAssertTrue; return this; } }

		public Assertion(T actual)
		{
			Actual = actual;
		}

		public static bool IsFailed(bool result, bool shouldAssertTrue)
		{
			bool resultIsFalseAndShouldAssertTrue = !result && shouldAssertTrue;
			bool resultIsTrueAndShouldAssertFalse = result && !shouldAssertTrue;
			return resultIsTrueAndShouldAssertFalse || resultIsFalseAndShouldAssertTrue;
		}

		public static void Verify(Assertion<T> assertion, Func<T, bool> result)
		{
			if (IsFailed(result.Invoke(assertion.Actual), assertion.shouldAssertTrue))
			{
				throw new AssertionExceptionUnary<T>(assertion.Actual, assertion.shouldAssertTrue, new System.Diagnostics.StackTrace().GetFrame(1).GetMethod());
			}
		}

		public static void Verify<U>(Assertion<T> assertion, U expected, Func<T, U, bool> result)
		{
			if (IsFailed(result.Invoke(assertion.Actual, expected), assertion.shouldAssertTrue))
			{
				throw new AssertionExceptionBinary<T, U>(assertion.Actual, expected, assertion.shouldAssertTrue, new System.Diagnostics.StackTrace().GetFrame(1).GetMethod());
			}
		}
	}
}