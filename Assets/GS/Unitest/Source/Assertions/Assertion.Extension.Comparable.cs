using System;

namespace GS.Unitest.Assertions
{
	public static class AssertionComparable
	{
		public static void IsLesserThan<T>(this Assertion<T> assertion, T expected) where T : IComparable
		{
			Assertion<T>.Verify(assertion, expected, (a, b) => object.Equals(a.CompareTo(b), -1));
		}

		public static void IsLesserThanOrEqualTo<T>(this Assertion<T> assertion, T expected) where T : IComparable
		{
			Assertion<T>.Verify(assertion, expected, (a, b) => object.Equals(a.CompareTo(b), -1) || object.Equals(a.CompareTo(b), 0));
		}

		public static void IsGreaterThan<T>(this Assertion<T> assertion, T expected) where T : IComparable
		{
			Assertion<T>.Verify(assertion, expected, (a, b) => object.Equals(a.CompareTo(b), 1));
		}

		public static void IsGreaterThanOrEqualTo<T>(this Assertion<T> assertion, T expected) where T : IComparable
		{
			Assertion<T>.Verify(assertion, expected, (a, b) => object.Equals(a.CompareTo(b), 1) || object.Equals(a.CompareTo(b), 0));
		}
	}
}