using System;
using System.Reflection;

namespace GS.Unitest.Assertions
{
	public abstract class AssertionException : Exception
	{
		public bool ShouldAssertTrue { get; protected set; }
		public MethodBase Method { get; protected set; }
	}

	public class AssertionExceptionUnary<T> : AssertionException
	{
		public T Actual { get; protected set; }

		public AssertionExceptionUnary(T actual, bool shouldAssertTrue, MethodBase method) : base()
		{
			ShouldAssertTrue = shouldAssertTrue;
			Method = method;
			Actual = actual;
		}

		public override string ToString()
		{
			string prefix = ShouldAssertTrue ? "" : "Not.";
			string method = Method.Name;
			string actual = Assertion.GetFormattedVariable(Actual);
			return string.Format("Assert.{0}{1} failed\nActual: <b><color=orange>{2}</color></b>", prefix, method, actual);
		}
	}

	public class AssertionExceptionBinary<T, U> : AssertionExceptionUnary<T>
	{
		public U Expected { get; protected set; }

		public AssertionExceptionBinary(T actual, U expected, bool shouldAssertTrue, MethodBase method) : base(actual, shouldAssertTrue, method)
		{
			Expected = expected;
		}

		public override string ToString()
		{
			string expected = Assertion.GetFormattedVariable(Expected);
			return string.Format("{0}\nExpected: <b><color=green>{1}</color></b>", base.ToString(), expected);
		}
	}
}