using System;

namespace GS.Unitest.Assertions
{
	public static class AssertionCallable
	{
		public static void Raises<E>(this Assertion<Action> assertion, string expectedMessage = null) where E : Exception
		{
			Exception exceptionRaised = null;
			try
			{
				assertion.Actual.Invoke();
			}
			catch (Exception exception)
			{
				exceptionRaised = exception;
			}

			Action expected = () => throw (E)Activator.CreateInstance(typeof(E), new object[] { expectedMessage });
			Assertion<Action>.Verify(assertion, expected, (actual, expected) =>
			{
				Exception exceptionActual = null;
				try
				{
					actual.Invoke();
				}
				catch (Exception exception)
				{
					exceptionActual = exception;
				}

				Exception exceptionExpected = null;
				try
				{
					expected.Invoke();
				}
				catch (Exception exception)
				{
					exceptionExpected = exception;
				}

				return exceptionActual != null && exceptionExpected != null
				&& object.Equals(exceptionActual.GetType(), exceptionExpected.GetType())
				&& object.Equals(exceptionActual.Message, exceptionExpected.Message);
			});
		}
	}
}