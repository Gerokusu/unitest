namespace GS.Unitest.Assertions
{
	public static class AssertionNullable
	{
		public static void IsReference<T>(this Assertion<T> assertion, T expected) where T : class
		{
			Assertion<T>.Verify(assertion, expected, object.ReferenceEquals);
		}

		public static void IsNull<T>(this Assertion<T> assertion) where T : class
		{
			Assertion<T>.Verify(assertion, (actual) => actual == null);
		}
	}
}