using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GS.Unitest
{
	public partial class TestStage
	{
		protected static TestStage Current;

		public void Parse()
		{
			TestStage parent = Current;
			Current = this;
			Current.Callback();
			Current = parent;
		}

		public static TestStage BuildFromAssemblies(IEnumerable<Assembly> assemblies)
		{
			TestStage stage = new TestStage()
			{
				Name = "root",
				Children = assemblies
				.SelectMany(assembly => assembly.GetTypes())
				.Where(type => type.IsClass && !type.IsAbstract && type.IsVisible && type.IsSubclassOf(typeof(TestStage)))
				.Select(typeTestStage => (TestStage)Activator.CreateInstance(typeTestStage))
				.ToList()
			};

			stage.Children.ForEach(child =>
			{
				child.Callback = () => child.Test();
				child.Parse();
			});

			return stage;
		}
	}
}