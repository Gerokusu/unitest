using Castle.DynamicProxy;
using GS.Unitest.Assertions;

namespace GS.Unitest.Mocks
{
	public class Mock<T> where T : class
	{
		private static ProxyGenerator Generator { get; } = new ProxyGenerator();
		protected MockInterceptor<T> Interceptor { get; set; }

		public T Spy(string method)
		{
			return Generator.CreateClassProxy<T>(Interceptor = new MockInterceptor<T>(method));
		}

		public object[][] GetCalls()
		{
			return Interceptor.Calls.ToArray();
		}

		public override string ToString()
		{
			string type = typeof(T).FullName;
			string method = Interceptor != null ? Interceptor.Method : "null";
			string calls = Assertion.GetFormattedVariable(GetCalls());
			return string.Format("{0}.{1} called with {2}", type, method, calls);
		}
	}
}