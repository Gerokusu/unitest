using System.Collections.Generic;
using Castle.DynamicProxy;

namespace GS.Unitest.Mocks
{
	public class MockInterceptor<T> : IInterceptor
	{
		public string Method { get; protected set; }
		public List<object[]> Calls { get; protected set; }

		public MockInterceptor(string method)
		{
			Method = method;
			Calls = new List<object[]>();
		}

		public void Intercept(IInvocation invocation)
		{
			if (invocation.Method.Name == Method)
			{
				Calls.Add(invocation.Arguments);
			}
			else
			{
				invocation.Proceed();
			}
		}
	}
}