namespace GS.Unitest
{
	public partial class TestStage
	{
		public string Name { get; protected set; }

		protected TestStage()
		{
			Name = GetType().FullName;
		}

		protected virtual void Test() { }
	}
}