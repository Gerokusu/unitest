using System;

namespace GS.Unitest
{
	public class TestStageEventArgs : EventArgs
	{
		public TestStageStatus Status { get; set; }
		public string Message { get; set; }
		public bool IsError { get; set; }
		public float Duration { get; set; }

		public TestStageEventArgs(TestStageStatus status, string message, bool isError, float duration) : base()
		{
			Status = status;
			Message = message;
			IsError = isError;
			Duration = duration;
		}
	}
}