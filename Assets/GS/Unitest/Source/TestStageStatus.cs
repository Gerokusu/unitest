namespace GS.Unitest
{
	public enum TestStageStatus
	{
		Failed,
		Ignored,
		Inconclusive,
		Normal,
		Passed,
		Stopwatch
	}
}