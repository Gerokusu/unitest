using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GS.Unitest.Assertions;

namespace GS.Unitest
{
	public partial class TestStage
	{
		protected TestStageStatus Status { get; set; } = TestStageStatus.Inconclusive;
		protected string Message { get; set; } = "";
		protected bool IsError { get; set; } = false;
		protected Stopwatch Stopwatch { get; set; } = new Stopwatch();
		public List<Assertion> Assertions { get; protected set; } = new List<Assertion>();
		public event Action<TestStage, TestStageEventArgs> OnStatusUpdate;

		public void TriggerStatusUpdate()
		{
			OnStatusUpdate?.Invoke(this, new TestStageEventArgs(Status, Message, IsError, Stopwatch.ElapsedTicks / (float)TimeSpan.TicksPerMillisecond));
		}

		public virtual void Run()
		{
			Status = TestStageStatus.Inconclusive;
			Stopwatch.Restart();
			if (Children.Count > 0)
			{
				RunWhen();
			}
			else
			{
				RunIt();
			}
			Stopwatch.Stop();
			TriggerStatusUpdate();
		}

		public void RunWhen()
		{

			int childrenFailedCount = Children
			.Select(child =>
			{
				child.Run();
				return child.Status;
			})
			.ToList()
			.Count(result => result != TestStageStatus.Passed && result != TestStageStatus.Ignored);

			if (childrenFailedCount == 0)
			{
				Status = TestStageStatus.Passed;
				Message = string.Format("{0} test{1} passed", Children.Count, Children.Count > 1 ? "s" : "");
				IsError = false;
			}
			else
			{
				int childrenPassedCount = Children.Count - childrenFailedCount;
				Status = TestStageStatus.Failed;
				Message = string.Format("{0} test{1} passed, {2} test{3} failed", childrenPassedCount, childrenPassedCount > 1 ? "s" : "", childrenFailedCount, childrenFailedCount > 1 ? "s" : "");
				IsError = false;
			}
		}

		public void RunIt()
		{
			Assertions.Clear();

			try
			{
				Current = this;
				CallbackBeforeEach?.Invoke();
				Callback();
				CallbackAfterEach?.Invoke();
				Status = Assertions.Count > 0 ? TestStageStatus.Passed : TestStageStatus.Ignored;
				Message = string.Format("{0} assertion{1} passed", Assertions.Count, Assertions.Count > 1 ? "s" : "");
				IsError = false;
			}
			catch (AssertionException exception)
			{
				Status = TestStageStatus.Failed;
				Message = exception.ToString();
				IsError = false;
			}
			catch (Exception exception)
			{
				Status = TestStageStatus.Failed;
				Message = string.Format("UNHANDLED {0}: {1}", exception.GetType().FullName, exception.Message);
				IsError = true;
			}
		}

		protected static Assertion<T> Assert<T>(T actual)
		{
			Assertion<T> assertion = new Assertion<T>((T)actual);
			Current.Assertions.Add(assertion);
			return assertion;
		}

		protected static Assertion<Action> Assert(Action actual)
		{
			Assertion<Action> assertion = new Assertion<Action>(actual);
			Current.Assertions.Add(assertion);
			return assertion;
		}
	}
}