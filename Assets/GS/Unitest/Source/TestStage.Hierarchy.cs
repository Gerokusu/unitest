using System;
using System.Collections.Generic;

namespace GS.Unitest
{
	public partial class TestStage
	{
		public List<TestStage> Children { get; protected set; } = new List<TestStage>();
		public Action CallbackBeforeEach { get; protected set; }
		public Action Callback { get; protected set; }
		public Action CallbackAfterEach { get; protected set; }

		protected static void BeforeEach(Action action)
		{
			Current.CallbackBeforeEach = action;
		}

		protected static void AfterEach(Action action)
		{
			Current.CallbackAfterEach = action;
		}

		protected static void When(string name, Action action)
		{
			TestStage stage = new TestStage() { Name = name, Callback = action };
			stage.Parse();

			Add(stage);
		}

		protected static void It(string name, Action action)
		{
			Add(new TestStage() { Name = name, Callback = action });
		}

		private static void Add(TestStage stage)
		{
			Action beforeEachParent = Current.CallbackBeforeEach;
			Action beforeEachChild = stage.CallbackBeforeEach;
			stage.CallbackBeforeEach = () =>
			{
				beforeEachParent?.Invoke();
				beforeEachChild?.Invoke();
			};

			Action afterEachParent = Current.CallbackAfterEach;
			Action afterEachChild = stage.CallbackAfterEach;
			stage.CallbackAfterEach = () =>
			{
				afterEachChild?.Invoke();
				afterEachParent?.Invoke();
			};

			Current.Children.Add(stage);
		}
	}
}